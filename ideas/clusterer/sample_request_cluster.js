// generated on client

const request = {
  configuration: {
    model: 'cluster',
    expected_responses: ["polygon"],
    input: {
      geom: {
        type:  "Feature",
        foreign_field: "points"
      }  
    },  
  },
  data: [
    {
      points: [{
      "geometry": {
          "coordinates": [
            31.138547658920285,
            -26.32319099110113
          ],
          "type": "Point"
        },
        "properties": {},
        "type": "Feature"
      }]
    }
  ]
}