// static definition (shared)

// client gets definition to construct request
// model gets definition
// checks input

const definition = {
  name: "cluster_v1",
  description: "this is a fine clusterer",
  model_path: "super_cluster.py",
  input: {
    geom: {
      type: "Point"
    }  
  },
  response_options: [
    {
      name: "bunch_of_polygons",
      description: "A bunch of polygons",
      type: "geom_polygons"
    }
  ]
}


// or type: 'basic_stats'