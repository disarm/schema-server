// generated on client

const request = {
  configuration: {
    model: 'prevalence_mapper_v16',
    expected_responses: ["average", "basic_stats"],
    input: {
      population_tested: {
        type: "number",
        foreign_field: 'num_people'
      },
      positive_observed: {
        type: "number",
        foreign_field: 'num_cases'
      }, 
      geom: {
        type:  "Feature",
        foreign_field: "points"
      }  
    },  
  },
  data: [
    {
      num_people: 32,
      num_cases: 16,
      points: [{
      "geometry": {
          "coordinates": [
            31.138547658920285,
            -26.32319099110113
          ],
          "type": "Point"
        },
        "properties": {},
        "type": "Feature"
      }]
    }
  ]
}