// static definition (shared)

// client gets definition to construct request
// model gets definition
// checks input

const definition = {
  name: "prevalence_mapper_v16",
  description: "this is a fine model",
  model_path: "super_model.py",
  input: {
    population_tested: {
      type: "number"
    },
    positive_observed: {
      type: "number"
    }, 
    geom: {
      type: "Feature"
    }  
  },
  response_options: [
    {
      name: "average",
      description: "the average",
      type: "number",
      function: "mean"
    },
    {
      name: "some_basic_stats",
      description: "basic stats",
      type: "basic_stats"
    }
  ]
}


// or type: 'basic_stats'