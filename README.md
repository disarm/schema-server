# The DiSARM Schema server

The purpose of this server is to provide json schemas for all DiSARM tools to validate against. The schemas can be found in the `models` folder.

We use JSON schema draft 4 as it is the most widely supported version. 

Generate the schemas on [jsonschema.net](https://jsonschema.net/#/editor)


## Libraries

Validate your json against a schema using the following libraries.

##### Javascript

[jsonschema](https://www.npmjs.com/package/jsonschema)


##### Python

[jsonschema](https://pypi.python.org/pypi/jsonschema)


##### R

[validatejsonr](https://cran.r-project.org/web/packages/validatejsonr/index.html)


## Geojson

Instead of relying on json schema for validating geojson it is recommended to use a package in your language to check the geojson is valid. 

##### Javascript 

Use the [geojson-validation](https://www.npmjs.com/package/geojson-validation) npm package.

##### Python 

Use the [geojson](https://pypi.python.org/pypi/geojson#validation) library. It provides an `is_valid` property on the output of every function call.  

##### R

[Geojsonio](https://cran.r-project.org/web/packages/geojsonio/index.html) has built in validation.

##### HTTP

[GeojsonLint](http://geojsonlint.com) is a web service that provides validation of geojson. POST your geojson to `http://geojsonlint.com/validate`.