const fs = require('fs')
const path = require('path')
const jsonschmema = require('jsonschema').Validator;

const validator = new jsonschmema();
const request_schema_path = path.join(__dirname, '..', 'internal_schemas', 'request_schema.json')
const request_schema = JSON.parse(fs.readFileSync(request_schema_path))

function validate_model_request(model_request) {
  const validation_result = validator.validate(model_request, request_schema)
  return validation_result
}

module.exports = {
  validate_model_request
}