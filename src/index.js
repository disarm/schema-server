const express = require('express')
const bodyParser = require('body-parser');

// const {validate_model_request} = require("./validate_model_request")
const {read_model_files} = require('./read_model_files')
const {run_model} = require('./run_model')

const app = express()

app.use(bodyParser.json())

app.get('/', function (req, res) {
  res.send('DiSARM schema server')
})

app.get('/models', function (req, res) {
  const models = read_model_files()
  res.send({models})
})

app.post('/run', async function (req, res) {
  const model_definition = req.body
  // const validation_result = validate_model_request(model_definition)
  //
  // if (validation_result.errors.length) {
  //   return res.status(401).send({error: validation_result.errors})
  // }

  const model_result = await run_model(model_definition)
  res.send(model_result)
})

app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})