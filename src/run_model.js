const path = require('path')
const child_process = require('child_process')
const fetch = require('node-fetch')
const StringDecoder = require('string_decoder').StringDecoder;

const {find_model_definition} = require("./read_model_files")
const fileExtension = require('file-extension')

async function run_model(model_request) {
  const model_definition = find_model_definition(model_request.configuration.model)

  const model_fn = get_function_for_language(model_definition.model_path)

  return await model_fn(model_request, model_definition.model_path)
}


function get_function_for_language(model_path) {
  console.log('model_path', model_path)
  const extension = fileExtension(model_path)
  
  console.log('extension', extension)

  switch (extension) {
    case 'py':
      return run_python
    case 'r':
      return run_R
    case 'js':
      return run_js
    default:
      return run_http
  }
}

const algorithm_path = path.join(__dirname, '..', 'local_algorithms')

async function run_python(model_request, model_path) {
  const res = child_process.execSync(`python ${model_path} ${JSON.stringify(model_request)}`, {cwd: algorithm_path})
  return get_output_from_fn(res)
}

async function run_R(model_request, model_path) {
  const res = child_process.execSync(`Rscript ${model_path} ${JSON.stringify(model_request)}`, {cwd: algorithm_path})
  return get_output_from_fn(res)
}

async function run_js(model_request, model_path) {
  const res = child_process.execSync(`node ${model_path} ${JSON.stringify(model_request)}`, {cwd: algorithm_path})
  return get_output_from_fn(res)
}

async function run_http(model_request, model_path) {
  const response = await fetch(model_path, {method: 'POST', body: model_request})
  const data = await response.json()
  return data
}

function get_output_from_fn(input) {
  var decoder = new StringDecoder('utf8');
  return decoder.write(input)
}

module.exports = {
  run_model,
  get_function_for_language,

  run_python,
  run_R,
  run_js,
  run_http

}