const fs = require('fs')
const path = require('path')

function read_model_files () {
  const models = []

  const files_to_join = ['definition.json', 'input_schema.json', 'output_schema.json']

  const model_directories = fs.readdirSync(path.join(__dirname, '..', 'models'))

  for (const model_dir of model_directories) {
    const model = {}
    for (const model_file of files_to_join) {
      const file_key_name = model_file.replace('.json', '')
      model[file_key_name] = JSON.parse(fs.readFileSync(path.join(__dirname, '..', 'models', model_dir, model_file)))
    }
    models.push(model)
  }

  return models
}

function find_model_definition (model_name) {
  const model_path = path.join(__dirname, '..', 'models', model_name, 'definition.json')
  try {
    const file = fs.readFileSync(model_path)
    return JSON.parse(file)
  } catch (e) {
    console.log(e)
  }
}

module.exports = {
  read_model_files,
  find_model_definition
}